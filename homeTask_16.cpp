﻿#include <iostream>
#include <time.h>


int main()
{
    const int n = 6;

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int today = buf.tm_mday;

    int numberOfString = today % n;

    int sumOfSrting = 0;

    int array[n][n];

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";

            if (i == numberOfString - 1) {
                sumOfSrting += array[i][j];
            }
        }
        std::cout << "\n";
    }

    std::cout << "Current date: " << buf.tm_mday << "\n";
    std::cout << "Current number of string: " << numberOfString << "\n";
    std::cout << "Sum of numbers in string: " << sumOfSrting << "\n";
}
